﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class PathFinder
    {
        CityPosition cityPos;
        MapNode[,] costMap;

        MapNode startNode;
        MapNode endNode;

        CityPosition.Direction[] Path;
        MapNode[] tilePath;

        public PathFinder(CityPosition pos)
        {
            cityPos = pos;
            costMap = new MapNode[City.XMax, City.YMax];

        }

        /// <summary>
        /// Will calculate the cost of each node
        /// </summary>
        public CityPosition.Direction[] GetDirections(Coordinate start, Coordinate end)
        {
            // Erase all past searches and start fresh.
            costMap = new MapNode[City.XMax, City.YMax];
            
            for (int x = 0; x < City.XMax; x++)
                for (int y = 0; y < City.YMax; y++)
                    costMap[x, y] = new MapNode(cityPos.CityTiles[x, y], x, y);



            startNode = costMap[start.XPos, start.YPos];
            endNode = costMap[end.XPos, end.YPos];

            HashSet<MapNode> openList = new HashSet<MapNode>();
            HashSet<MapNode> closedList = new HashSet<MapNode>();

            MapNode currentSquare = startNode;

            List<CityPosition.Direction> path = new List<CityPosition.Direction>();

            int steps = 0;



            while (!currentSquare.SameCoordinatesAs(endNode) && steps < City.YMax * City.XMax)
            {
                closedList.Add(currentSquare);
                if (openList.Contains(currentSquare)) openList.Remove(currentSquare);

                MapNode[] possibleNext = GetAdjacentNodes(currentSquare);
                foreach (MapNode n in possibleNext)
                {
                    if (n.type != CityPosition.TileType.Building & n.type != CityPosition.TileType.Wall)
                    {
                        if (!closedList.Contains(n))
                        {
                            if (!openList.Contains(n))
                            {
                                // Compute the scores
                                n.GCost = steps + 1;
                                n.HCost = getManhattanDistance(n);
                                openList.Add(n);
                            }
                            else
                            {
                                n.GCost = steps + 1;
                            }
                        }
                    }
                }

                // Now, calculate where we should go next
                if (openList.Contains(endNode))
                {
                    // We got there!
                    closedList.Add(endNode);
                    currentSquare = endNode;
                    break;
                }

                if (openList.Count == 0)
                {
                    throw new Exception("No paths found.");
                }

                MapNode nextPossible = openList.Last();
                for (int i = openList.Count - 1; i >= 0; i--)
                {
                    if (openList.ElementAt(i).FCost < nextPossible.FCost)
                        nextPossible = openList.ElementAt(i);
                }

                currentSquare = nextPossible;
                steps++;
            }

            // We found a path. Now update it based on the closed list
            MapNode node = endNode;
            List<MapNode> tNodes = new List<MapNode>();
            while (!node.Coordinate.Equals(start) && path.Count < 100)
            {
                if (node.PreviousNode.X < node.X)
                    path.Add(CityPosition.Direction.Right);
                else if (node.PreviousNode.X > node.X)
                    path.Add(CityPosition.Direction.Left);
                else if (node.PreviousNode.Y < node.Y)
                    path.Add(CityPosition.Direction.Up);
                else if (node.PreviousNode.Y > node.Y)
                    path.Add(CityPosition.Direction.Down);

                tNodes.Add(node);
                node = node.PreviousNode;
            }

            tilePath = tNodes.ToArray();
            Path = path.ToArray();

            return Path;
        }

        MapNode[] GetAdjacentNodes(MapNode currentNode)
        {
            int x = currentNode.X;
            int y = currentNode.Y;

            ObservableCollection<MapNode> nodes = new ObservableCollection<MapNode>();

            if (x > 0) nodes.Add(costMap[x - 1, y]);
            if (x < City.XMax - 1) nodes.Add(costMap[x + 1, y]);
            if (y > 0) nodes.Add(costMap[x, y - 1]);
            if (y < City.YMax - 1) nodes.Add(costMap[x, y + 1]);

            foreach (var n in nodes)
                if (n.PreviousNode == null)
                    n.PreviousNode = currentNode;

            return nodes.ToArray();
        }

        int getManhattanDistance(MapNode node)
        {
            // Will calculate the probably best distance between the two points, given no obstructions
            return Math.Abs(node.X - endNode.X) + Math.Abs(node.Y - endNode.Y);
        }

        public string GetPathView()
        {
            StringBuilder sb = new StringBuilder();

            for (int y = 0; y < City.YMax; y++)
            {
                string row = "";
                for (int x = 0; x < City.XMax; x++)
                {
                    string stCost = "  ";
                    if (costMap[x, y].type == CityPosition.TileType.Wall || costMap[x,y].type == CityPosition.TileType.Building) stCost = " ■";
                    if (tilePath.Contains(costMap[x, y])) stCost = " ○";
                    row += stCost;
                }
                sb.AppendLine(row);
            }

            return sb.ToString();
        }

        class MapNode
        {
            public MapNode(CityPosition.TileType mapType, int x, int y, int hcost = -1, int gcost = -1)
            {
                type = mapType;
                HCost = hcost;
                GCost = gcost;
                this.Coordinate = new Coordinate(x, y);
            }

            public int HCost;
            public int GCost;

            public int FCost
            {
                get { return HCost + GCost; }
            }

            public CityPosition.TileType type;
            public Coordinate Coordinate;
            public int X
            {
                get { return Coordinate.XPos; }
                set { Coordinate.SetXPos(value); }
            }
            public int Y
            {
                get { return Coordinate.YPos; }
                set { Coordinate.SetYPos(value); }
            }
            public MapNode PreviousNode;

            public bool SameCoordinatesAs(object obj)
            {
                if (obj is MapNode)
                {
                    MapNode n = obj as MapNode;
                    return (n.X == X && n.Y == Y);
                }

                return false;
            }
        }
    }
}
