﻿
namespace TransportProj
{
    public class CityPosition
    {
        public TileType[,] CityTiles;

        public CityPosition(City city, Car car = null, Passenger passenger = null)
        {
            this.city = city;
            Car = car;
            Passenger = passenger;
            CityTiles = new TileType[City.XMax, City.YMax];

            // Initialize the map
            for(int y = 0; y < City.YMax; y++)
            {
                for (int x = 0; x < City.XMax; x++)
                {
                    CityTiles[x, y] = TileType.Road;
                }
            }
        }



        public City city { get; set; }
        public Car Car { get; set; }
        public Passenger Passenger { get; set; }


        public Coordinate GetCarPos()
        {
            return new Coordinate(Car.XPos, Car.YPos);
        }

        /// <summary>
        /// Gets the current position of the passenger.
        /// </summary>
        /// <returns></returns>
        public Coordinate GetPassengerCoordinate()
        {
            if (Passenger.Car != null)
                return GetCarPos();
            else
                return new Coordinate(Passenger.StartingXPos, Passenger.StartingYPos);
        }

        public void AddBuilding(Coordinate startCoords, int width, int height)
        {
            // Set all these coordinates to a building
            for (int x = startCoords.XPos; x <= startCoords.XPos + width; x++)
                for (int y = startCoords.YPos; y <= startCoords.YPos + height; y++)
                    if (x < City.XMax && y < City.YMax)
                        CityTiles[x, y] = TileType.Building;
        }
        
        public bool IsSpaceFree(Coordinate coords)
        {
            return CityTiles[coords.XPos, coords.YPos] == TileType.Road;
        }


        public enum TileType
        {
            Wall, Road, Building
        }

        public enum Direction
        {
            Left, Right, Up, Down
        }
    }


}
