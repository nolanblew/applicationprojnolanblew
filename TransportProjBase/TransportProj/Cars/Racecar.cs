﻿using System;

namespace TransportProj
{
    public class Racecar : Car
    {
        public Racecar(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
            Speed = 3;
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos += Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos -= Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos += Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos -= Speed;
                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
