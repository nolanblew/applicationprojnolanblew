﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TransportProj
{
    public class Sedan : Car
    {

        int stepCount = 0;
        PathFinder pf;
        Queue<CityPosition.Direction> drivingDirections;

        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
            pf = new PathFinder(city.Position);
            Speed = 1;
            drivingDirections = new Queue<CityPosition.Direction>();
        }

        public void GetDirections(Coordinate destination)
        {
            drivingDirections = new Queue<CityPosition.Direction>(pf.GetDirections(this, destination));
            System.Windows.Forms.Clipboard.SetText(pf.GetPathView());
        }

        public bool MovePerDirections()
        {
            if (drivingDirections.Count == 0)
                return false;

            CityPosition.Direction nextDirection = drivingDirections.Dequeue();
            switch(nextDirection)
            {
                case CityPosition.Direction.Up:
                    MoveUp();
                    break;
                case CityPosition.Direction.Down:
                    MoveDown();
                    break;
                case CityPosition.Direction.Left:
                    MoveLeft();
                    break;
                case CityPosition.Direction.Right:
                    MoveRight();
                    break;
            }

            return true;
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos += Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos -= Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos += Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos -= Speed;
                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
