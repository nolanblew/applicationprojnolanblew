﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class CarFactory
    {
        Dictionary<string, Car> Cars;
        City City;

        public CarFactory(City city)
        {
            Cars = new Dictionary<string, Car>();
            City = city;
        }

        public Car GetCar(string carName)
        {
            if (Cars.ContainsKey(carName))
                return Cars[carName];
            else
                return null;
        }

        public Car GetRandomCar() {
            Random rnd = new Random();
            return Cars.ElementAt(rnd.Next(0, Cars.Count - 1)).Value;
        }

        public Car MakeCar(string name, int xPos, int yPos, int speed = 0)
        {
            if (Cars.ContainsKey(name))
                throw new Exception("Name already exists.");

            if (speed < 0 || speed > 100)
                throw new Exception("Speed out of range. Must be between 1 and 100.");

            if (speed == 0)
            {
                Random rnd = new Random();
                speed = Math.Max(1, (int)Math.Pow(75, rnd.NextDouble()));
            }

            Car newCar = new BaseCar(xPos, yPos, City, null, speed);
            Cars.Add(name, newCar);

            return newCar;

        }
    }
}
