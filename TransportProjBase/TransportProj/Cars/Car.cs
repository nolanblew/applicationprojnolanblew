﻿using System;

namespace TransportProj
{
    public abstract class Car : Coordinate
    {
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }
        public int Speed { get; set; }

        public Car(int xPos, int yPos, City city, Passenger passenger, int speed = 1) :
            base(xPos, yPos)
        {
            City = city;
            Passenger = passenger;
            Speed = speed;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
            Passenger.Car = this;
            Console.WriteLine("Car picked up passenger.");
        }

        /// <summary>
        /// Moves the car towards a point at it's maximum speed. Will slow down to ensure it doesn't pass it's point.
        /// </summary>
        /// <param name="destX">The destination X point to move to</param>
        /// <param name="destY">The destination Y point to move to</param>
        public void MoveTowardsPoint(int destX, int destY)
        {
            int originalSpeed = Speed;

            if (destX != XPos)
            {
                Speed = Math.Min(Speed, Math.Abs(destX - XPos));
                if (destX < XPos)
                    MoveLeft();
                else if (destX > XPos)
                    MoveRight();
            }
            else if (destY != YPos)
            {
                Speed = Math.Min(Speed, Math.Abs(destY - YPos));
                if (destY < YPos)
                    MoveDown();
                else if (destY > YPos)
                    MoveUp();
            }

            Speed = originalSpeed;
        }

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();
    }
}
