﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{

    public class Coordinate
    {
        public Coordinate(int x, int y)
        {
            XPos = x;
            YPos = y;
        }

        public int XPos { get; protected set; }
        public int YPos { get; protected set; }

        public void SetXPos (int X)
        {
            XPos = X;
        }
        public void SetYPos(int Y)
        {
            YPos = Y;
        }

        public override string ToString()
        {
            return string.Format("({0},{1})", XPos, YPos);
        }

        public override bool Equals(object obj)
        {
            if (obj is Coordinate)
            {
                Coordinate c = obj as Coordinate;
                return (c.XPos == XPos && c.YPos == YPos);
            }

            return false;
        }
    }
}
