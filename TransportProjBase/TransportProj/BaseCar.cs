﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class BaseCar : Car
    {
        public BaseCar(int xPos, int yPos, City city, Passenger passenger, int speed) : base(xPos, yPos, city, passenger, speed)
        {
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos += Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos -= Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos += Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos -= Speed;
                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
