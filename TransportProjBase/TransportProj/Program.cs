﻿using System;
using Nito.AsyncEx;
using System.Threading.Tasks;

namespace TransportProj
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            AsyncContext.Run(() => MainFun());
        }

        public async static void MainFun()
        {
            Random rand = new Random(DateTime.Now.Ticks.ToString().GetHashCode());
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);

            MyCity.AddBuildingToCity(5, 2, 2, 2);
            MyCity.AddBuildingToCity(1, 3, 2, 2);
            MyCity.AddBuildingToCity(2, 7, 2, 2);
            Coordinate c = new Coordinate((rand.Next(CityLength - 1)), rand.Next(CityWidth - 1));
            while (!MyCity.Position.IsSpaceFree(c))
                c = new Coordinate((rand.Next(CityLength - 1)), rand.Next(CityWidth - 1));

            Car car = MyCity.AddCarToCity(c.XPos, c.YPos);


            c = new Coordinate((rand.Next(CityLength - 1)), rand.Next(CityWidth - 1));
            while (!MyCity.Position.IsSpaceFree(c))
                c = new Coordinate((rand.Next(CityLength - 1)), rand.Next(CityWidth - 1));

            Coordinate c1 = new Coordinate((rand.Next(CityLength - 1)), rand.Next(CityWidth - 1));
            while (!MyCity.Position.IsSpaceFree(c1))
                c1 = new Coordinate((rand.Next(CityLength - 1)), rand.Next(CityWidth - 1));

            Passenger passenger = MyCity.AddPassengerToCity(c.XPos, c.YPos, c1.XPos, c1.YPos);


            Console.WriteLine("You will be driving a car with speed of " + car.Speed + "!");

            while (!passenger.IsAtDestination())
            {
                await Tick(car, passenger);
            }

            Console.WriteLine("Dropped off passenger successfully.");
            Console.ReadKey();
            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static async Task Tick(Car car, Passenger passenger)
        {
            Sedan car_s = car as Sedan;

            // If we are where the passenger is, pick up the passenger
            if (car.Passenger == null && car.XPos == passenger.StartingXPos && car.YPos == passenger.StartingYPos)
            {
                car.PickupPassenger(passenger);
                car_s.GetDirections(new Coordinate(passenger.DestinationXPos, passenger.DestinationYPos));
                return;
            }

            int destX = car.Passenger == null ? passenger.StartingXPos : passenger.DestinationXPos;
            int destY = car.Passenger == null ? passenger.StartingYPos : passenger.DestinationYPos;

            // Get the car to where it's going
            // First, drive left/right to where our destination is
            bool isMoveSuccess = car_s.MovePerDirections();
            if (car.Passenger == null && !isMoveSuccess)
            {
                car_s.GetDirections(new Coordinate(destX, destY));
                car_s.MovePerDirections();
            } else if (passenger != null && isMoveSuccess) {

                // Passenger is moving and wants to waste all their data downloading the 2PointB website!
                await new System.Net.WebClient().DownloadStringTaskAsync(new Uri("http://2pointb.com/"));
            }

        }

    }
}
