﻿
using System;

namespace TransportProj
{
    public class City
    {
        CarFactory factory;
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }

        public CityPosition Position;

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
            factory = new CarFactory(this);
            Position = new CityPosition(this);
        }

        public Car AddCarToCity(int xPos, int yPos)
        {
            //Car car = factory.MakeCar(DateTime.Now.Ticks.ToString(), xPos, yPos);
            Car car = new Sedan(xPos, yPos, this, null);
            Position.Car = car;

            return car;
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);
            Position.Passenger = passenger;

            return passenger;
        }

        public void AddBuildingToCity(int startX, int startY, int width, int height)
        {
            Position.AddBuilding(new Coordinate(startX, startY), width, height);
        }

    }
}
